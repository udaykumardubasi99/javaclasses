package javaDocs;

/**
* The HelloWorld program implements an application that
* simply displays "Hello World!" to the standard output.
*
* @author  Uday Kumar
* @version 1.0
* @since   2020-06-26 
*/
public class helloworld {
	public static void main(String[] args) {
	      // Prints Hello, World! on standard output.
	      System.out.println("Hello World!");
	   }

}
