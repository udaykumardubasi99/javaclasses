package exceptionExample;

public class ThrowsDemo {

	public static void main(String args[]) {
		try {
		   ThrowingClass.meth();
		      }
		// The main() method catches and handles it
		catch (ClassNotFoundException e) {
		   System.out.println(e);
		    }
		 }
}
