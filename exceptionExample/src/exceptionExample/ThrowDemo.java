package exceptionExample;

public class ThrowDemo {
	public static void main(String args[]){
		
		
		String input = "invalid input";
		try {
		   if (input.equals("invalid input")) {
		      throw new RuntimeException("This is correct code ==");
		   } else {
		      System.out.println(input);
		   }
		   System.out.println("After throwing");
	      } catch (RuntimeException e) {
	         System.out.println("Exception caught:" + e);
	      }   
		   
	} 
}
