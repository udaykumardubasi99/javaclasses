package exceptionExample;

public class FinallyDemo {
	
	static void myMethod(int n) throws Exception{
		
		try {
			switch(n) {
			
			 case 1:System.out.println("case 1");
			        return;
			 case 3:System.out.println("case 3");
			        throw new RuntimeException("3!");
			 case 4:System.out.println("case 4");
			        throw new Exception("4!");
			 case 2:System.out.println("case 2");
			
			}
			
		}catch (RuntimeException e){
			
			System.out.print("RuntimeException: ");
            System.out.println(e.getMessage());
		}
		finally {
            System.out.println("try-block entered.");
        }
	}
	
	

}
