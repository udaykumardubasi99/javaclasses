package com.runtime.example;

public class Dog extends Animal{

	
	@Override
    public void sound(){
        System.out.println("the dog barks");
    }
    public static void main(String args[]){
    	Animal obj = new Dog();
    	obj.sound();
    }
}
