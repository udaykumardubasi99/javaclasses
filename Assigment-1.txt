﻿1. 
Write  a program to converts Celsius to Fahrenheit Temperature.


public class CtoF
{
 public static void main (String[] args)
 {
   // Celsius temp is a constant
   // Fahrenheit temp

   final double CELS = 37;
   double FAHR;

   // calculate Fahrenheit temp using formula
   // display intro message

   System.out.println("This program converts Celsius to Fahrenheit");
   // display the result

   System.out.println("Celsius Temp = " + CELS);
   System.out.println("Fahrenheit Temp = " + FAHR);

   // display end message
   System.out.println("End of program");
 }
}


2. 
Create a new Java file named Pay.java to calculate gross pay based on user's hours worked and hourly pay rate.


public class Pay
{
      public static void main(String [] args)
   {
   // Declare Variables
      double hoursWorked;      
      double hourlyPayRate;
      double grossPay;
   // take the following variables from the user from command promt.
   …...
System.out.print("How many hours did you work? ");
hoursWorked = // read from the command line
System.out.print("How much do you get paid per hour? ");
hourlyPayRate = // read from the command line
…...


// Display the results
 System.out.println("You earned $" + grossPay)
    }
}


1. After reading the input from the keyboard calculate the gross pay by using the following formula.
a.) if num of hours is less than or equal to 40 then gross pay is hours worked times the hourly pay rate.
b.) if num of hours worked is greater than 40 then gross pay is hours worked times 1.5 times the hourly pay rate.
c.) While the Pay program may now calculate gross pay correctly. Assume that the tax man takes a flat 33% of gross wages as taxes. Write down the math formula needed to calculate taxes and net pay assuming that 33% of gross pay is deducted as taxes
 
taxes = write out the formula   
netPay = write out the formula

Modify the Pay program to calculate and display taxes and net pay.
Percentages are entered as double values in Java, so 33% would be 0.33 in your program.
Recompile and re-run your program, comparing the answers it produces to your hand-calculated test cases.


Sample Output
How many hours did you work? 15
How much do you get paid per hour? 10.00
You earned $150.0
Taxes are $49.5
Your net pay is $100.5






3. Write a Java program that can serve as a ending time calculator. The user enters the starting time in hours and minutes, a duration in total minutes, and your program will calculate and display the ending time (as hours:minutes).
For example, if an event starts at 2 30 and lasts 125 minutes, it will end at 4 35
Note: To simplify the problem, assume military time (0..23) rather than standard time, in which you would need to worry about a.m. and p.m.


Use integer multiplication, division, and modulus operators
The algorithm to solve this problem on a computer is not straight-forward. Here is a solution:
a. Convert the starting time in hours and minutes to the equivalent total minutes
* For example: 2 hours and 30 minutes would be 150 total minutes
* For example: 5 hours and 15 minutes would be 315 total minutes
* Hint: use multiplication and addition
Write down the math formula needed to calculate the starting time (in total minutes) given the start hours and start minutes.
startingTime (in total minutes) = write out the formula

b. Add the duration (in minutes) to this starting time (in total minutes), giving you the ending time in total minutes
* For example: a start time of 150 (in total minutes) plus 125 minutes duration duration gives you an ending time of 275 total minutes
* For example: a start time of 315 (in total minutes) plus 10 minutes duration gives you an ending time of 325 total minutes
Write down the math formula needed to calculate the ending time (in total minutes) given the starting time (in total minutes) and the duration (in minutes).
endingTime (in total minutes) = write out the formula

c. Convert the ending time in total minutes back to hours and minutes with / and % integer operators and the fact that one hour is 60 minutes
* For example, 63 minutes is 1 hour and 3 minutes
* For example, 125 minutes is 2 hours and 5 minutes
* For example, 325 minutes is 5 hours and 25 minutes


Write down the math formula needed to calculate the ending hour and minutes given the ending time (in total minutes).
Note: at this point, assume simple military time for the end hours. So 12:50 plus 30 minutes would be 13:20
endingHour = write out the formula
endingMinutes = write out the formula

public class EndingTime
{
   public static void main(String [] args)
   {      
   // Variable declarations
   // Hint: All variables need to be declared as integers
   
   // Create a Scanner object to read from the keyboard
      Scanner keyboard = new Scanner(System.in);
      
   // Get the starting time in hours and minutes
   
   // Get the duration time in minutes
   
   // Calculate the ending time   
   
   // Display the output
   }
}


Sample Session
Enter the starting time (in hours and minutes): 2 30
Enter the duration (in minutes): 125
Ending hour is 4 
Ending minute is 35