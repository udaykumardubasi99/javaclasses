
public class MathDemo {

	public static void main(String args[]) {
	      System.out.println("absolute value of -5: " + Math.abs(-5));
	      System.out.println("absolute value of 5: " + Math.abs(5));
	      System.out.println("random number(max is 10): " + Math.random()*10);
	      System.out.println("max of 3.5 and 1.2: " + Math.max(3.5,1.2));
	      System.out.println("min of 3.5 and 1.2: " + Math.min(3.5,1.2));
	      
	      System.out.println("ceiling of 3.5: " + Math.ceil(3.5));  // 4.0
	      System.out.println("floor of 3.5: " + Math.floor(3.5)); // 3.0
	      System.out.println("e raised to 1: " + Math.exp(1));
          System.out.println("log 10: " + Math.log(10));
          System.out.println("10 raised to 3: " + Math.pow(10,3));
	}
	
	
}
