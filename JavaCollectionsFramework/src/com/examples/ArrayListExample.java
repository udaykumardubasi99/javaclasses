package com.examples;

import java.util.ArrayList;

public class ArrayListExample {
	
	 public static void main(String args[]){  
	  ArrayList<String> alist=new ArrayList<String>();  
	  alist.add("Steve");
      alist.add("Tim");
      alist.add("Lucy");
      alist.add("Pat");
      alist.add("Angela");
      alist.add("Tom");
  
      //displaying elements
      System.out.println(alist);
  
      //Adding "Steve" at the fourth position
      alist.add(3, "Steve");
  
      //displaying elements
      System.out.println(alist);
      
    /* Remove operation 
      //Removing "Steve" and "Angela"
      alist.remove("Steve");
      alist.remove("Angela");

      //displaying elements
      System.out.println(alist);
      
      */
      
      /* //iterating ArrayList
      for(String str:alist)  
          System.out.println(str);  */
      
      //System.out.println("Number of elements in ArrayList: "+alist.size());
      
   }  
	
}
