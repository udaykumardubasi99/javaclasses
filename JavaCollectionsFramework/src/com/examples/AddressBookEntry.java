package com.examples;

public class AddressBookEntry implements Comparable<AddressBookEntry> {
	   private String name, address, phone;
	 
	   public AddressBookEntry(String name) {  // constructor, ignore address and phone
	      this.name = name;
	   }
	 
	   @Override
	   public String toString() {  // describe itself
	      return name;
	   }
	 
	   @Override
	   public int compareTo(AddressBookEntry other) {  // Interface Comparable<T>
	      return this.name.compareToIgnoreCase(other.name);
	   }
	 
	   @Override
	   public boolean equals(Object o) {
	      return o != null && o instanceof AddressBookEntry && this.name.equalsIgnoreCase(((AddressBookEntry)o).name);
	   }
	 
	   // Two objects which are equals() shall have the same hash code
	   @Override
	   public int hashCode() {
	      return name.toLowerCase().hashCode();
	   }
	}
