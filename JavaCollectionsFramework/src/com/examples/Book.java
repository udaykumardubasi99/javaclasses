package com.examples;

public class Book {
	private int id;
	   private String title;
	 
	   public Book(int id, String title) {  // constructor
	      this.id = id;
	      this.title = title;
	   }
	 
	   @Override
	   public String toString() {  // describle itself
	      return id + ": " + title;
	   }
	 
	   // Two books are equal if they have the same id
	   @Override
	   public boolean equals(Object o) {
	      return o != null && o instanceof Book && this.id == ((Book)o).id;
	   }
	 
	   // To be consistent with equals(). Two objects which are equal have the same hash code.
	   @Override
	   public int hashCode() {
	      return id;
	   }
}
