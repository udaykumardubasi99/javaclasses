package com.iostreamexample;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;

public class bufferedOutputStreamExample {

	public static void main(String[] args) {

        String data = "This is a demo of the flush method";

        try {
            // Creates a FileOutputStream
            FileOutputStream file = new FileOutputStream("/Users/udaykumardubasi/Desktop/Material/myJavaPrograms/iostreamexample/outputclass/flush1.txt");

            // Creates a BufferedOutputStream
            BufferedOutputStream buffer = new BufferedOutputStream(file);

            // Writes data to the output stream
            buffer.write(data.getBytes());

            // Flushes data to the destination
            buffer.flush();
            System.out.println("Data is flushed to the file.");
            buffer.close();
        }

        catch(Exception e) {
            e.getStackTrace();
        }
    }
}
