package com.iostreamexample;
import java.io.FileWriter;
import java.io.Writer;

public class writerClassExample {
	
	public static void main(String args[]) {

        String data = "This is the data in the output file";

        try {
            // Creates a Writer using FileWriter
            Writer output = new FileWriter("/Users/udaykumardubasi/Desktop/Material/myJavaPrograms/iostreamexample/outputclass/output.txt");

 
            // Writes string to the file
            output.write(data);
            
            System.out.println("Data is writen into file successfully");

            // Closes the writer
            output.close();
        }

        catch (Exception e) {
            e.getStackTrace();
        }
    }

}
