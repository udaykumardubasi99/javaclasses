package com.iostreamexample;

import java.io.FileOutputStream;
import java.io.IOException;
public class FileOutputStreamExample {

	public static void main(String[] args) throws IOException {

        FileOutputStream out = null;
        String data = "This is demo of flush method";

        try {
            out = new FileOutputStream("/Users/udaykumardubasi/Desktop/Material/myJavaPrograms/iostreamexample/outputclass/flush.txt");

            // Using write() method
            out.write(data.getBytes());
            System.out.println("System is about to flush the data ");
            // Using the flush() method
            out.flush();
            
            out.close();
        }
        catch(Exception e) {
            e.getStackTrace();
        }
    }
}
