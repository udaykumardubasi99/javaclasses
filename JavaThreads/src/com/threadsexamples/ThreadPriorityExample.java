package com.threadsexamples;

public class ThreadPriorityExample extends Thread{
	public void run() 
	{ 
		System.out.println("Thread Running..."); 
	} 

	public static void main(String[]args) 
	{ 
		ThreadPriorityExample p1 = new ThreadPriorityExample(); 
		ThreadPriorityExample p2 = new ThreadPriorityExample(); 
		ThreadPriorityExample p3 = new ThreadPriorityExample(); 
		p1.start();
		System.out.println("P1 thread priority : " + p1.getPriority()); 
		System.out.println("P2 thread priority : " + p2.getPriority());  
		System.out.println("P3 thread priority : " + p3.getPriority()); 
		System.out.println("max thread priority : " + p1.MAX_PRIORITY); 
		System.out.println("min thread priority : " + p1.MIN_PRIORITY);  
		System.out.println("normal thread priority : " + p1.NORM_PRIORITY); 
		System.out.println("thread priority : " + p1.getPriority());
		        
		
	} 
}
