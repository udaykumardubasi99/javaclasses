package com.threadsexamples;

public class ThreadNameExample extends Thread {
	
	public void run()
    {
       System.out.println("thread running...");
    }
    public static void main(String[] args)
    {
    	ThreadNameExample t1=new ThreadNameExample();
        t1.start();
        System.out.println("thread name: "+t1.getName());
        t1.setName("mythread");
        System.out.println("Thread Name: "+t1.getName());
    }

}
