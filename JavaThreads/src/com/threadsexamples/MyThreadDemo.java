package com.threadsexamples;

class MyThread extends Thread
{
 	public void run()
 	{
  		System.out.println("concurrent thread started running..");
 	}
}


public class MyThreadDemo {

	public static void main(String args[])
 	{
  		MyThread mt = new  MyThread();
  		mt.start();
  	
 	}
}
