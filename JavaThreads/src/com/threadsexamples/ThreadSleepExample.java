package com.threadsexamples;

public class ThreadSleepExample extends Thread {
	
	ThreadSleepExample(String str){
        super(str);
    }

    public void run()
    {
            System.out.println(Thread.currentThread().getName()+" Started");
            try{
            	ThreadSleepExample.sleep(1500); 
                }catch(InterruptedException ie){
                    System.out.println(ie);
            }
            System.out.println(Thread.currentThread().getName()+" Finished");
    }
    public static void main(String[] args)
    {
    	ThreadSleepExample t1=new ThreadSleepExample("first thread");
    	ThreadSleepExample t2=new ThreadSleepExample("second thread");
            t1.start();
            t2.start();
    }

}
