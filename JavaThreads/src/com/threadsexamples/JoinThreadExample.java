package com.threadsexamples;

public class JoinThreadExample extends Thread{

	public void run()
   	{
       		System.out.println("r1");
       		try {
        		Thread.sleep(500);
    		}
    		catch(InterruptedException ie){ }
       		System.out.println("r2 ");
  	}
	public static void main(String[] args)
	{
		JoinThreadExample ta=new JoinThreadExample();
		JoinThreadExample tb=new JoinThreadExample();
		ta.start();
		/*try{
  			ta.join();	//Waiting for ta to finish
		}catch(InterruptedException ie){}*/

		tb.start();
		System.out.println(ta.isAlive());
		System.out.println(tb.isAlive());
		
	}
}
